"""design_thinking URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
	https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
	1. Add an import:  from my_app import views
	2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
	1. Add an import:  from other_app.views import Home
	2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
	1. Import the include() function: from django.conf.urls import url, include
	2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import patterns, url, include
from django.contrib import admin

from Core import views

admin.autodiscover()

urlpatterns = [
	url(r'^admin/', admin.site.urls),
	url(r'^', include('Core.urls')),
	url(r'^map', views.ShowZonen, name='map'),
	# url(r'^lol', views.import_db, name='lol'),
	url(r'^login', views.login, name='login'),
	url(r'^pro$', views.pro, name='pro'),
	url(r'^product', views.product, name='product'),
	url(r'^formation_id', views.formation_id, name='formation_id'),
	url(r'^formateur_id', views.formateur_id, name='formateur_id'),
	url(r'^wip', views.wip, name='wip'),

	# url(r'^map', views.mapi, name='map'),
	# url(r'^formation$', views.ShowZonen, name='formations'),

]
