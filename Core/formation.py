from __future__ import unicode_literals
from django.db import models
from geoposition.fields import GeopositionField
import datetime

class Zone_prev(models.Model):
	name			= models.CharField(max_length = 50 )
	number_and_road	= models.CharField(max_length = 300 )
	city			= models.CharField(max_length = 100 )
	zip_code		= models.CharField(max_length = 100 )
	position 		= GeopositionField()

	def __str__(self):
		return self.name


class Zone_test(models.Model):
	name		= models.CharField(max_length = 50)
	address		= models.CharField(max_length = 50)
	zip_code	= models.CharField(max_length = 50)
	tel			= models.CharField(max_length = 50)
	city		= models.CharField(max_length = 50)
	lat			= models.CharField(max_length = 50)
	longitude	= models.CharField(max_length = 50)

	def __str__(self):
		return self.name