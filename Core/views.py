from django.shortcuts import render, render_to_response, get_object_or_404, redirect
from Core.formation import Zone_test


# Create your views here.
def index(request):
	return render(request, 'Core/index.html')
def login(request):
	return render(request, 'Core/login.html')
def pro(request):
	return render(request, 'Core/pro.html')
def product(request):
	return render(request, 'Core/product.html')
def wip(request):
	return render(request, 'Core/wip.html')
def formation_id(request):
	return render(request, 'Core/formation_id.html')
def formateur_id(request):
	return render(request, 'Core/formateur_id.html')
def ShowZonen(request):
	zone_test=Zone_test.objects.all()
	return render_to_response('Core/map.html', {"zone_test": zone_test})


def import_db(request):
	f = open('/Users/tbouder/Desktop/import.csv', 'r')  
	for line in f:
		line = line.split(',')
		tmp = Zone_test.objects.create()
		tmp.name		= line[0]
		tmp.address		= line[1]
		tmp.zip_code	= line[2]
		tmp.tel			= line[3]
		tmp.city		= line[4]
		tmp.lat			= line[5]
		tmp.longitude	= line[6]
		tmp.save()
	f.close()