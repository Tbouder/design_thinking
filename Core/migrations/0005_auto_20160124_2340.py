# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-01-24 23:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Core', '0004_auto_20160122_2321'),
    ]

    operations = [
        migrations.CreateModel(
            name='Zone_test',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Lat', models.CharField(max_length=50)),
                ('Lng', models.CharField(max_length=50)),
            ],
        ),
        migrations.RenameModel(
            old_name='Zone',
            new_name='Zone_prev',
        ),
    ]
