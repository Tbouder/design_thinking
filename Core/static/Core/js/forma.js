var map; /* map */
var geocoder = new google.maps.Geocoder(); /* geocoder */

/* global google */
function launch() {
	var latlng = new google.maps.LatLng(34.070264, -118.4440562);
	var myOptions = 
	{
		zoom: 13,
		panControl:false,
		zoomControl:true,
		mapTypeControl:false,
		scaleControl:false,
		streetViewControl:false,
		overviewMapControl:false,
		rotateControl:false,
		center: {lat:48.896539, lng: 2.318376}, // 42
		styles: [
			{"featureType":"Road","elementType":"labels","stylers":[{"visibility":"off"}]},
			{"featureType":"landscape","elementType":"all","stylers":[{"color":"#efebe2"}]},
			{"featureType":"poi","elementType":"all","stylers":[{"color":"#efebe2"}]},
			{"featureType":"poi.attraction","elementType":"all","stylers":[{"color":"#efebe2"}]},
			{"featureType":"poi.business","elementType":"all","stylers":[{"color":"#efebe2"}]},
			{"featureType":"poi.government","elementType":"all","stylers":[{"color":"#dfdcd5"}]},
			{"featureType":"poi.medical","elementType":"all","stylers":[{"color":"#dfdcd5"}]},
			{"featureType":"poi.park","elementType":"all","stylers":[{"color":"#bad294"}]},
			{"featureType":"poi.place_of_worship","elementType":"all","stylers":[{"color":"#efebe2"}]},
			{"featureType":"poi.school","elementType":"all","stylers":[{"color":"#efebe2"}]},
			{"featureType":"poi.sports_complex","elementType":"all","stylers":[{"color":"#efebe2"}]},
			{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},
			{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},
			{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},
			{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},
			{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"#fbfbfb"}]},
			{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},
			{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},
			{"featureType":"water","elementType":"all","stylers":[{"color":"#a5d7e0"}]}
		]
	};
	map = new google.maps.Map(document.getElementById('map_canvas'),myOptions);

	// Try HTML5 geolocation.
	if (navigator.geolocation)
	{
		navigator.geolocation.getCurrentPosition(function(position)
		{
			var pos =
			{
				lat: position.coords.latitude,
				lng: position.coords.longitude
			};
			map.setCenter(pos);
		},
		function()
		{
			handleLocationError(true, infoWindow, map.getCenter());
			addMarker(49,2);
		});
	} 
	else
	{
		// Browser doesn't support Geolocation
		handleLocationError(false, infoWindow, map.getCenter());
	}
// {% for mark in zone %}
var test = "{{ mark.city }}";
window.alert(test);
		geocode("{{ mark.city }}");

// {% endfor %}
}

//geocode function
function geocode(address) 
{
	// var address = $('#address').val();
	geocoder.geocode( { 'address': address}, function(results, status) {
	if (status == google.maps.GeocoderStatus.OK) 
	{
		map.setCenter(results[0].geometry.location);
		var marker = new google.maps.Marker({
		map: map, 
		position: results[0].geometry.location
		});
	} 
	else 
	{
		alert("Geocode was not successful for the following reason: " + status);
	}
	});
}

function handleLocationError(browserHasGeolocation, infoWindow, pos)
{
	infoWindow.setPosition(pos);
	infoWindow.setContent(browserHasGeolocation ?
	'Erreur: La géolocalisation a échouée.' :
	'Erreur: Votre navigateur ne supporte pa la géolocalisation.');
}

google.maps.event.addDomListener(window, 'load', launch);

