/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gmaps_bg.js                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Tbouder <Tbouder@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/26 13:37:36 by Tbouder           #+#    #+#             */
/*   Updated: 2016/01/26 21:56:00 by Tbouder          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

google.maps.event.addDomListener(window, 'load', init);
function init() 
{
	var mapOptions = 
	{
		zoom: 9,
		panControl:false,
		zoomControl:false,
		mapTypeControl:false,
		scaleControl:false,
		streetViewControl:false,
		overviewMapControl:false,
		rotateControl:false,
		scrollwheel: false,
		draggable: false,
		center: {lat:47.507555, lng: 2.318376}, // 42
		styles: [
			{"featureType":"All","elementType":"labels","stylers":[{"visibility":"off"}]},
			{"featureType":"landscape","elementType":"all","stylers":[{"color":"#efebe2"}]},
			{"featureType":"poi","elementType":"all","stylers":[{"color":"#efebe2"}]},
			{"featureType":"poi.attraction","elementType":"all","stylers":[{"color":"#efebe2"}]},
			{"featureType":"poi.business","elementType":"all","stylers":[{"color":"#efebe2"}]},
			{"featureType":"poi.government","elementType":"all","stylers":[{"color":"#dfdcd5"}]},
			{"featureType":"poi.medical","elementType":"all","stylers":[{"color":"#dfdcd5"}]},
			{"featureType":"poi.park","elementType":"all","stylers":[{"color":"#bad294"}]},
			{"featureType":"poi.place_of_worship","elementType":"all","stylers":[{"color":"#efebe2"}]},
			{"featureType":"poi.school","elementType":"all","stylers":[{"color":"#efebe2"}]},
			{"featureType":"poi.sports_complex","elementType":"all","stylers":[{"color":"#efebe2"}]},
			{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},
			{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},
			{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},
			{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},
			{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"#fbfbfb"}]},
			{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},
			{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},
			{"featureType":"water","elementType":"all","stylers":[{"color":"#a5d7e0"}]}
		]
	};
	var mapElement = document.getElementById('map');
	var map = new google.maps.Map(mapElement,mapOptions);
}
