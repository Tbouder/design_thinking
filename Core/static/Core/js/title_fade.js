/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   title_fade.js                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Tbouder <Tbouder@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/21 22:47:02 by Tbouder           #+#    #+#             */
/*   Updated: 2016/01/26 21:55:55 by Tbouder          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/* V3 : Hide logo, title and slogan after 3600 */	
setTimeout(function()
{
	$('.title').fadeOut(1000);
}, 2600);

setTimeout(function()
{
	$('.slogan').fadeOut(1000);
}, 2600);

/* V1 : Display search bar after logo and co */	
$(function()
{
	$("#wrap").hide();
});

setTimeout(function()
{
	$('#wrap').fadeIn(1000);
}, 3600);
